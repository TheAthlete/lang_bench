#include "rapidjson/document.h"
#include "rapidjson/reader.h"
#include <iostream>

using namespace rapidjson;
using namespace std;

int convert(const char *str) {
    Document d;
    d.Parse(str);

    int sum = 0;
    for (auto& v: d.GetArray()) {
      for (auto &m : v.GetObject()) {
        sum += m.value.GetInt();
      }
    }
    return sum;
}

int main() {
    const char* json = R"json(
      [ {"a": 1}, {"b": 2}, {"c": 3}]
      )json";

    int sum = convert(json);

    cout << "sum = " << sum << endl;
    
    return 0;
}
